<?php date_default_timezone_set("Europe/Minsk"); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Главная</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/hover.css" rel="stylesheet">
    <link href="css/material-kit.css" rel="stylesheet">

    <link rel="shortcut icon" href="images/favicon.png" type="image/png">

    <script src="//cdn.blitz-market.ru/sprite/latest/"></script>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="/js/clipboard.min.js"></script>

    <link rel="stylesheet" href="plugins/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <script src="plugins/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>

    <script src="js/material.min.js"></script>
    <script src="js/material-kit.js"></script>

    <link href="style.css" rel="stylesheet">
</head>

<body>
<div class="wrapper">
    <div class="content">
        <div class="page-wrapper">
            <div class="total-bg">
                <div class="wrapp">
                    <div class="line-bg">
                        <div class="line l1"></div>
                        <div class="line l2 show1000"></div>
                        <div class="line l3 show1000"></div>
                        <div class="line l4"></div>
                        <div class="line l5 show1000"></div>
                        <div class="line l6 show1000"></div>
                        <div class="line l7"></div>
                    </div>
                </div>
            </div>
        </div>
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-xs-2">
                        <div class="logo">
                            <a href="//blitz-market.ru/" target="_blank"><img src="images/logo.png" alt=""></a>
                        </div>
                    </div>

                    <div class="col-xs-10">
                        <ul class="main_menu">
                            <li><a class="active" href="/">Иконки</a></li>
                            <li><a href="how_use.php">Как подключить</a></li>
                            <li><a href="designers.php">Дизайнерам</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <section class="sprite">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="main_title">Иконки</p>
                        <p class="caption">Бесплатный набор svg иконок платежных систем, валют и прочего</p>
                        <p class="title">Платежные системы цветные</p>

                        <div class="copy_block1">
                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_paykassa"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_payeer"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_perfectmoney"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_advcash"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_qiwi"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_yandexmoney"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_card"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_bitcoin"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_litecoin"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_ethereum"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_dash"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_dogecoin"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_ripple"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_zcash"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_nixmoney"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_okpay"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_bitcoincash"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_ethereumclassic"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_waves"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_neo"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_iota"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_monero"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_nem"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_bitcoingold"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_bytecoin"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_cardano"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_lisk"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_nano"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_qtum"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_steem"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_colored_stellar"></use>
                                </svg>
                            </div>

                            <p id="example"></p>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <p class="title">Платежные системы под покраску</p>

                        <div class="thumbnail">
                            <div class="caption">
                                <div class="form-group label-floating is-empty">
                                    <label>Цвет:</label>
                                    <div class="input-group colorpicker">
                                        <input id="color" type="text" class="form-control" value="#000000">
                                        <div class="input-group-addon">
                                            <i></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="copy_block">
                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_paykassa"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_payeer"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_perfectmoney"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_advcash"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_qiwi"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_yandexmoney"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_card"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_bitcoin"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_litecoin"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_ethereum"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_dash"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_dogecoin"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_ripple"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_zcash"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_nixmoney"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_okpay"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_bitcoincash"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_ethereumclassic"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_waves"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_neo"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_iota"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_monero"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_nem"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_bitcoingold"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_bytecoin"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_cardano"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_lisk"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_nano"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_qtum"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_steem"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_payment_transparent_stellar"></use>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <p class="title">Платежные системы mini цветные</p>

                        <div class="copy_block1">
                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_paykassa"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_yandexmoney"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_qiwi"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_nixmoney"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_perfectmoney"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_litecoin"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_payeer"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_okpay"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_ethereum"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_dogecoin"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_advcash"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_bitcoin"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_card"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_zcash"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_ripple"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_dash"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_bitcoincash"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_ethereumclassic"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_neo"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_nem"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_monero"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_iota"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_waves"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_bitcoingold"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_bytecoin"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_cardano"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_lisk"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_nano"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_qtum"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_steem"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_colored_stellar"></use>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <p class="title">Платежные системы mini под покраску</p>

                        <div class="thumbnail">
                            <div class="caption">
                                <div class="form-group label-floating is-empty">
                                    <label>Цвет:</label>
                                    <div class="input-group colorpicker">
                                        <input id="color2" type="text" class="form-control" value="#000000">
                                        <div class="input-group-addon">
                                            <i></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="copy_block2">
                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_paykassa"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_yandexmoney"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_qiwi"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_nixmoney"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_perfectmoney"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_litecoin"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_payeer"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_okpay"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_ethereum"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_dogecoin"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_advcash"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_bitcoin"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_card"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_zcash"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_ripple"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_dash"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_bitcoincash"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_ethereumclassic"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_neo"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_nem"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_monero"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_iota"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_waves"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_bitcoingold"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_bytecoin"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_cardano"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_lisk"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_nano"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_qtum"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_steem"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_mini_logos_transparent_stellar"></use>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <p class="title">Значки валют</p>

                        <div class="thumbnail">
                            <div class="caption">
                                <div class="form-group label-floating is-empty">
                                    <label>Цвет:</label>
                                    <div class="input-group colorpicker">
                                        <input id="color3" type="text" class="form-control" value="#000000">
                                        <div class="input-group-addon">
                                            <i></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="copy_block3">
                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_slim_rub"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_rub"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_slim_eur"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_eur"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_slim_btc"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_btc"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_slim_usd"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_usd"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_slim_doge"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_doge"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_slim_zec"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_zec"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_slim_ltc"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_ltc"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_eth"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_dash"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_slim_bch"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_bch"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_etc"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_neo"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_nem"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_xmr"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_iota"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_waves"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_cardano"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_lisk"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_nano"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_qtum"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_currency_bold_steem"></use>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <p class="title">Цветные иконки социальных сетей</p>

                        <div class="copy_block">
                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_colored_youtube"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_colored_vk"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_colored_twitter"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_colored_telegram"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_colored_skype"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_colored_instagram"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_colored_facebook"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_colored_mmgp"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_colored_reddit"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_colored_ok"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_colored_whatsapp"></use>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <p class="title">Социальные сети с окрашиваемым логотипом</p>

                        <div class="thumbnail">
                            <div class="caption">
                                <div class="form-group label-floating is-empty">
                                    <label>Цвет:</label>
                                    <div class="input-group colorpicker">
                                        <input id="color5" type="text" class="form-control" value="#000000">
                                        <div class="input-group-addon">
                                            <i></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="copy_block5">
                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_empty_social_youtube"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_empty_social_vk"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_empty_social_twitter"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_empty_social_telegram"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_empty_social_skype"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_empty_social_instagram"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_empty_social_facebook"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_empty_social_mmgp"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_empty_social_reddit"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_empty_social_bit"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_empty_social_ok"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_empty_social_whatsapp"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_empty_social_email"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_empty_social_phone"></use>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <p class="title">Социальные сети с окрашенной областью вокруг значка</p>

                        <div class="copy_block">
                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_colored_empty_social_youtube"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_colored_empty_social_vk"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_colored_empty_social_twitter"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_colored_empty_social_telegram"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_colored_empty_social_skype"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_colored_empty_social_instagram"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_colored_empty_social_facebook"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_colored_empty_social_mmgp"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_colored_empty_social_reddit"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_colored_empty_social_ok"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_colored_empty_social_whatsapp"></use>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <p class="title">Социальные сети с окрашиваемой областью вокруг значка</p>

                        <div class="thumbnail">
                            <div class="caption">
                                <div class="form-group label-floating is-empty">
                                    <label>Цвет:</label>
                                    <div class="input-group colorpicker">
                                        <input id="color4" type="text" class="form-control" value="#000000">
                                        <div class="input-group-addon">
                                            <i></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="copy_block4">
                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_youtube"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_vk"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_twitter"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_telegram"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_skype"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_instagram"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_facebook"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_mmgp"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_reddit"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_bit"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_ok"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_whatsapp"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_email"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_social_phone"></use>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <p class="title">Флаги стран</p>

                        <div class="copy_block1">
                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_sa"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_ie"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_nz"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_en_england"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_ua"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_za"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_id"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_pt"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_hi"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_th"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_kr"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_jp"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_it"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_es"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_en"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_fi"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_dk"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_no"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_cz"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_gr"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_il"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_se"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_tr"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_cn"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_ru"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_de"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_fr"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_flags_en_united_kingdom"></use>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <p class="title">Специальные значки</p>

                        <div class="thumbnail">
                            <div class="caption">
                                <div class="form-group label-floating is-empty">
                                    <label>Цвет:</label>
                                    <div class="input-group colorpicker">
                                        <input id="color6" type="text" class="form-control" value="#000000">
                                        <div class="input-group-addon">
                                            <i></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="copy_block6">
                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_special_place"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_special_email"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_special_close"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_special_success"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_special_phone"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_special_zoom"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_special_arrow"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#bm_icon_special_bm"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#success"></use>
                                </svg>
                            </div>

                            <div class="copy bg small hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="41" height="41" role="img">
                                    <use xlink:href="#error"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example" style="margin-right: 0;">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#blitz-host"></use>
                                </svg>
                            </div>
                        </div>
                    </div>

	                <div class="col-xs-12">
		                <p class="title">Цветные логотипы ssl-брендов</p>

		                <div class="copy_block">
				                <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
					                <svg width="170" height="60" role="img">
						                <use xlink:href="#bm_icon_ssl_colored_comodo"></use>
					                </svg>
				                </div>

				                <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
					                <svg width="170" height="60" role="img">
						                <use xlink:href="#bm_icon_ssl_colored_verisign"></use>
					                </svg>
				                </div>

				                <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
					                <svg width="170" height="60" role="img">
						                <use xlink:href="#bm_icon_ssl_colored_thawte"></use>
					                </svg>
				                </div>

				                <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
					                <svg width="170" height="60" role="img">
						                <use xlink:href="#bm_icon_ssl_colored_symantec"></use>
					                </svg>
				                </div>

				                <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
					                <svg width="170" height="60" role="img">
						                <use xlink:href="#bm_icon_ssl_colored_rapidssl"></use>
					                </svg>
				                </div>

				                <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
					                <svg width="170" height="60" role="img">
						                <use xlink:href="#bm_icon_ssl_colored_geotrust"></use>
					                </svg>
				                </div>

				                <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
					                <svg width="170" height="60" role="img">
						                <use xlink:href="#bm_icon_ssl_colored_entrust"></use>
					                </svg>
				                </div>

                                <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                    <svg width="170" height="60" role="img">
                                        <use xlink:href="#bm_icon_ssl_colored_digicert"></use>
                                    </svg>
                                </div>
			                </div>
		                </div>

                    <div class="col-xs-12">
                        <p class="title">Логотипы ssl-брендов под покраску</p>

                        <div class="thumbnail">
                            <div class="caption">
                                <div class="form-group label-floating is-empty">
                                    <label>Цвет:</label>
                                    <div class="input-group colorpicker">
                                        <input id="color7" type="text" class="form-control" value="#000000">
                                        <div class="input-group-addon">
                                            <i></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="copy_block7">
                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_ssl_transparent_comodo"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_ssl_transparent_verisign"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_ssl_transparent_thawte"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_ssl_transparent_symantec"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_ssl_transparent_rapidssl"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_ssl_transparent_geotrust"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_ssl_transparent_entrust"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_ssl_transparent_digicert"></use>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <p class="title">Цветные логотипы CMS</p>

                        <div class="copy_block">
                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_bitrix"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_wordpress"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_woocommerce"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_opencart"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_shop-script"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_amiro"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_cs-cart"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_diafan"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_hostcms"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_ips"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_joomla"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_magento"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_modx"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_moodle"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_vm"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_netcat"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_oscommerce"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_phpshop"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_prestashop"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_shopos"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_simpla"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_umi"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_vamshop"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_cms_colored_whmcs"></use>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <p class="title">Цветные логотипы прочих брендов</p>

                        <div class="copy_block">
                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_brands_colored_apache"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_brands_colored_cisco"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_brands_colored_cloudflare"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_brands_colored_cloudlinux"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_brands_colored_cpanel"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_brands_colored_mysql"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_brands_colored_nginx"></use>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <p class="title">Логотипы прочих брендов под покраску</p>

                        <div class="thumbnail">
                            <div class="caption">
                                <div class="form-group label-floating is-empty">
                                    <label>Цвет:</label>
                                    <div class="input-group colorpicker">
                                        <input id="color8" type="text" class="form-control" value="#000000">
                                        <div class="input-group-addon">
                                            <i></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="copy_block8">
                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_brands_transparent_apache"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_brands_transparent_cisco"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_brands_transparent_cloudflare"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_brands_transparent_cloudlinux"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_brands_transparent_cpanel"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_brands_transparent_mysql"></use>
                                </svg>
                            </div>

                            <div class="copy bg big hvr-sweep-to-top" data-clipboard-target="#example">
                                <svg width="170" height="60" role="img">
                                    <use xlink:href="#bm_icon_brands_transparent_nginx"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" id="message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <p>Скопировано в буфер обмена!</p>
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="copyright">All Rights Reserved. © Blitz-License | 2014 - <?php echo date('Y'); ?></div>
                </div>
            </div>
        </div>
    </footer>
</div>

<script>
    $(document).ready(function () {
        $('.copy').click(function () {
            $('#message').modal('show');
            setTimeout(function(){
                $('#message').modal('hide');
            }, 2000);
        });
    });
</script>

    <script>
    $(".colorpicker").colorpicker();

    $("input#color").on('input', function () {
        var color = $("#color").val();
        $(".copy_block .copy").find("svg").attr("fill", color);
    });

    $("#color").change(function () {
        var color = $("#color").val();
        $(".copy_block .copy").find("svg").attr("fill", color);
    });

    $(".copy_block .copy").click(function () {
        var el = ($(this).html()).trim();
        $("#example").text(el);
        new ClipboardJS('.copy');
    });



    $("input#color2").on('input', function () {
        var color = $("#color2").val();
        $(".copy_block2 .copy").find("svg").attr("fill", color);
    });

    $("#color2").change(function () {
        var color = $("#color2").val();
        $(".copy_block2 .copy").find("svg").attr("fill", color);
    });

    $(".copy_block2 .copy").click(function () {
        var el = ($(this).html()).trim();
        $("#example").text(el);
        new ClipboardJS('.copy');
    });



    $("input#color3").on('input', function () {
        var color = $("#color3").val();
        $(".copy_block3 .copy").find("svg").attr("fill", color);
    });

    $("#color3").change(function () {
        var color = $("#color3").val();
        $(".copy_block3 .copy").find("svg").attr("fill", color);
    });

    $(".copy_block3 .copy").click(function () {
        var el = ($(this).html()).trim();
        $("#example").text(el);
        new ClipboardJS('.copy');
    });



    $("input#color4").on('input', function () {
        var color = $("#color4").val();
        $(".copy_block4 .copy").find("svg").attr("fill", color);
    });

    $("#color4").change(function () {
        var color = $("#color4").val();
        $(".copy_block4 .copy").find("svg").attr("fill", color);
    });

    $(".copy_block4 .copy").click(function () {
        var el = ($(this).html()).trim();
        $("#example").text(el);
        new ClipboardJS('.copy');
    });



    $("input#color5").on('input', function () {
        var color = $("#color5").val();
        $(".copy_block5 .copy").find("svg").attr("fill", color);
    });

    $("#color5").change(function () {
        var color = $("#color5").val();
        $(".copy_block5 .copy").find("svg").attr("fill", color);
    });

    $(".copy_block5 .copy").click(function () {
        var el = ($(this).html()).trim();
        $("#example").text(el);
        new ClipboardJS('.copy');
    });



    $("input#color6").on('input', function () {
        var color = $("#color6").val();
        $(".copy_block6 .copy").find("svg").attr("fill", color);
    });

    $("#color6").change(function () {
        var color = $("#color6").val();
        $(".copy_block6 .copy").find("svg").attr("fill", color);
    });

    $(".copy_block6 .copy").click(function () {
        var el = ($(this).html()).trim();
        $("#example").text(el);
        new ClipboardJS('.copy');
    });



    $("input#color7").on('input', function () {
        var color = $("#color7").val();
        $(".copy_block7 .copy").find("svg").attr("fill", color);
    });

    $("#color7").change(function () {
        var color = $("#color7").val();
        $(".copy_block7 .copy").find("svg").attr("fill", color);
    });

    $(".copy_block7 .copy").click(function () {
        var el = ($(this).html()).trim();
        $("#example").text(el);
        new ClipboardJS('.copy');
    });



    $("input#color8").on('input', function () {
        var color = $("#color8").val();
        $(".copy_block8 .copy").find("svg").attr("fill", color);
    });

    $("#color8").change(function () {
        var color = $("#color8").val();
        $(".copy_block8 .copy").find("svg").attr("fill", color);
    });

    $(".copy_block8 .copy").click(function () {
        var el = ($(this).html()).trim();
        $("#example").text(el);
        new ClipboardJS('.copy');
    });



    $(".copy_block1 .copy").click(function () {
        var el = ($(this).html()).trim();
        $("#example").text(el);
        new ClipboardJS('.copy');
    });
</script>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter46586904 = new Ya.Metrika({
                        id:46586904,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true,
                        trackHash:true,
                        ecommerce:"dataLayer"
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/46586904" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

</body>
</html>