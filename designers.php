<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Дизайнерам</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/hover.css" rel="stylesheet">
    <link href="css/material-kit.css" rel="stylesheet">

    <link rel="shortcut icon" href="images/favicon.png" type="image/png">

    <script src="//cdn.blitz-market.ru/sprite/latest/"></script>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="//cdn.rawgit.com/zenorocha/clipboard.js/master/dist/clipboard.min.js"></script>

    <link rel="stylesheet" href="plugins/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <script src="plugins/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>

    <script src="js/material.min.js"></script>
    <script src="js/material-kit.js"></script>

    <link href="style.css" rel="stylesheet">
</head>

<body>
<div class="wrapper">
    <div class="content">
        <div class="page-wrapper">
            <div class="total-bg">
                <div class="wrapp">
                    <div class="line-bg">
                        <div class="line l1"></div>
                        <div class="line l2 show1000"></div>
                        <div class="line l3 show1000"></div>
                        <div class="line l4"></div>
                        <div class="line l5 show1000"></div>
                        <div class="line l6 show1000"></div>
                        <div class="line l7"></div>
                    </div>
                </div>
            </div>
        </div>
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-xs-2">
                        <div class="logo">
                            <a href="//blitz-market.ru/" target="_blank"><img src="images/logo.png" alt=""></a>
                        </div>
                    </div>

                    <div class="col-xs-10">
                        <ul class="main_menu">
                            <li><a href="/">Иконки</a></li>
                            <li><a href="how_use.php">Как подключить</a></li>
                            <li><a class="active" href="designers.php">Дизайнерам</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <section class="sprite">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="main_title">Дизайнерам</p>
                        <p class="caption">Для использования спрайтов для прочих целей скачайте архив с отдельными svg-иконками</p>
                        <div class="text">
                            <p class="title">Архив с отдельными файлами svg-иконок:</p>
                            <p>
                                Для скачивания архива кликните на значок "Скачать"
                                <a href="/archive/svg_for__designers.zip" class="download" download>
                                    <img src="/images/download.svg" alt="">
                                </a>
                            </p>
                            <b style="display: block; padding-top: 15px;">Дата последнего обновления: 13.07.2018</b>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" id="message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <p>Скопировано в буфер обмена!</p>
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="copyright">All Rights Reserved. © Blitz-License | 2014 - <?php echo date('Y'); ?></div>
                </div>
            </div>
        </div>
    </footer>
</div>

<script>
    $('#copybtn').click(function () {
        copy();
        $('#message').modal('show');
        setTimeout(function(){
            $('#message').modal('hide');
        }, 2000);
    });

    function copy() {
        var cutTextarea = document.querySelector('#copy');
        cutTextarea.select();
        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copy');
        } catch (err) {
            console.log('Oops, unable to cut');
        }
    }
</script>

</body>
</html>