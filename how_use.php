<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Как подключить</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/hover.css" rel="stylesheet">
    <link href="css/material-kit.css" rel="stylesheet">

    <link rel="shortcut icon" href="images/favicon.png" type="image/png">

    <script src="//cdn.blitz-market.ru/sprite/latest/"></script>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="//cdn.rawgit.com/zenorocha/clipboard.js/master/dist/clipboard.min.js"></script>

    <link rel="stylesheet" href="plugins/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
    <script src="plugins/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>

    <script src="js/material.min.js"></script>
    <script src="js/material-kit.js"></script>

    <link href="style.css" rel="stylesheet">
</head>

<body>
<div class="wrapper">
    <div class="content">
        <div class="page-wrapper">
            <div class="total-bg">
                <div class="wrapp">
                    <div class="line-bg">
                        <div class="line l1"></div>
                        <div class="line l2 show1000"></div>
                        <div class="line l3 show1000"></div>
                        <div class="line l4"></div>
                        <div class="line l5 show1000"></div>
                        <div class="line l6 show1000"></div>
                        <div class="line l7"></div>
                    </div>
                </div>
            </div>
        </div>
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-xs-2">
                        <div class="logo">
                            <a href="//blitz-market.ru/" target="_blank"><img src="images/logo.png" alt=""></a>
                        </div>
                    </div>

                    <div class="col-xs-10">
                        <ul class="main_menu">
                            <li><a href="/">Иконки</a></li>
                            <li><a class="active" href="how_use.php">Как подключить</a></li>
                            <li><a href="designers.php">Дизайнерам</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <section class="sprite">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="main_title">Как подключить</p>
                        <p class="caption">Инструкция по подключению svg-иконок из спрайт-листа</p>
                        <div class="text">
                            <p class="title">Вставка svg-элемента:</p>
                            <ul>
                                <li id="copybtn">В шапку проекта подлючить скрипт:
                                    <b >
                                        <input id="copy" type="text" value="&lt;script src=&quot;//cdn.blitz-market.ru/sprite/latest/&quot;&gt;&lt;/script&gt;" readonly>

                                    </b>
                                </li>
                                <li>Окрасить выбранный элемент с помощью <b>колорпикера</b> или посредством ввода неоходимого цвета в <b>input</b></li>
                                <li>Кликнуть на данный элемент для копирования html-кода</li>
                                <li>Вставить скопированный код в нужное место на странице проекта</li>
                            </ul>
                            <p class="title">Стилизация svg-элемента:</p>
                            <ul>
                                <li>Для переопределения размера элемента изменить значения атрибутов <b>width</b> и <b>height</b> тега svg на нужные, как то: <b>&lt;svg width="170" height="60" role="img"&gt;&lt;/svg&gt;</b></li>
                                <li>Для переопределения цвета изображений "под покраску" необходимо изменить атрибут <b>fill</b> тега svg, например: <b>&lt;svg fill="#ffffff" role="img"&gt;&lt;/svg&gt;</b></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" id="message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <p>Скопировано в буфер обмена!</p>
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="copyright">All Rights Reserved. © Blitz-License | 2014 - <?php echo date('Y'); ?></div>
                </div>
            </div>
        </div>
    </footer>
</div>

<script>
    $('#copybtn').click(function () {
        copy();
        $('#message').modal('show');
        setTimeout(function(){
            $('#message').modal('hide');
        }, 2000);
    });

    function copy() {
        var cutTextarea = document.querySelector('#copy');
        cutTextarea.select();
        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copy');
        } catch (err) {
            console.log('Oops, unable to cut');
        }
    }
</script>

</body>
</html>